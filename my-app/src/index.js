import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';

function SectionBC() {
    const [data, setData] = useState([]);
    const [hideSection, setHideSection] = useState(false)
    const url = '/community';

    useEffect(() => {
        fetch(url)
            .then((response) => {
                return response.json();
            })
            .then(setData)
            .catch((error) => {
                alert(error)
            });
    }, [])

    function handleClick(event) {
        if (!hideSection) {
            setHideSection(true);
        } else {
            setHideSection(false)
        }
    }

    function peopleReder(person) {
        return (
            <div key={person.name} className='app-people'>
                <img className='app-people--avatar' src={person.avatar} alt='Avatar' />
                <p className='app-people--description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.</p>
                <div className='app-people--names'>{person.firstName} {person.lastName}</div>
                <div className='app-people--post'>{person.position}</div>
            </div>
        )
    }

    return (
        <section className='app-section app-section--big-community'>
            <div className='app-text app-text--big-community'>
                <h2 className='app-title app-title--big-community'>
                    Big Community of People Like You
                </h2>
                <h3 className='app-subtitle app-subtitle--big-community'>
                    We’re proud of our products, and we’re really excited when we get feedback from our users.
                </h3>
            </div>
            <button className='app-toggle app-toggle--big-community' onClick={handleClick}>
                {hideSection ? 'Show section' : 'Hide section'}
            </button>
            <div className='app-people-information' style={hideSection ? { display: 'none' } : {}}>
                {
                    data.map((element) => {
                        return peopleReder(element)
                    })
                }
            </div>
        </section>

    )
}

function SectionJOP() {
    const [inputData, setInputData] = useState('')
    const [buttonStatus, setButtonStatus] = useState('submit')
    const [loading, setLoading] = useState(false)
    const urlSubscribe = '/subscribe'
    const urlUnsubscribe = '/unsubscribe'

    const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];
    function validate(email) {
        let result = false;
        for (let i = 0; i < VALID_EMAIL_ENDINGS.length; i++) {
            let item = VALID_EMAIL_ENDINGS[i];
            if (email.length >= item.length && email.endsWith(item)) {
                result = true;
            }
        }
        return result;
    }

    function sendRequest(url, data, callback) {
        fetch(url, {
            method: 'post',
            headers: {
                'Content-type': 'application/json',
                'Content-Security-Policy': 'script-src "*"'
            },
            body: JSON.stringify({ email: data }),
        })
            .then((response) => {
                if (response.status === 200) {
                    if (callback) {
                        return callback(true)
                    }
                } else if (response.status === 422) {
                    response.json().then((data) => {
                        alert(data.error)
                        callback(false)
                    })
                }
            })
    }

    function handleChange(event) {
        setInputData(event.target.value);
    }


    function handleSubmit(event) {
        event.preventDefault();
        console.log(inputData)
        if (buttonStatus !== 'unsubscribe') {
            if (validate(inputData)) {
                setLoading(true)
                sendRequest(urlSubscribe, inputData, (result) => {
                    if (result) {
                        setButtonStatus('unsubscribe')
                    }
                    setLoading(false)
                })
            }
        } else {
            setLoading(true);
            sendRequest(urlUnsubscribe, inputData, (result) => {
                if (result) {
                    setButtonStatus('subscribe')
                }
                setLoading(false)
            })
        }
    }

    return (
        <section className='app-section app-section--join-program'>
            <div className="app-text app-text--join-program">
                <h2 className="app-title app-title--join-program">Join Our Program</h2>
                <h3 className="app-subtitle app-subtitle--join-program">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>
            </div>
            <form className="app-input-menu" onSubmit={handleSubmit}>
                <input onChange={handleChange} style={buttonStatus === 'unsubscribe' ? { display: 'none' } : {}} disabled={loading} name='email' className="app-input" type="text" placeholder="Email" />
                <input disabled={loading} className="app-section__button app-section__button--join-program" type="submit" value={buttonStatus} />
            </form>
        </section>
    )
}





const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <div id='app-container'>
        <SectionBC />
        <SectionJOP />
    </div>

);
